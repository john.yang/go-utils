// @system SHOP-YUN <shop-yun.com>
// @version shop-yun 1.0
// @author xiaotangren  <unphp@qq.com>
// @data 2014-07-21
// 公共接口

package base

import (
//"fmt"
)

type StackChaner interface {
	Stack(string)
	Get() ([]string, bool)
}

type Log interface {
	Println(string)
}

type InterfaceSafeLog interface {
	Init(params *SafeLogParams)
	Create()
	Add(s string)
	Find() (value []string, found bool)
	Delete()
}
