// 最顶级路由---type
// @version go-utils 0.1
// @author xiaotangren  <unphp@qq.com>
// @data 2014-07-21
package socket

import (
	"git.oschina.net/unphp/go-utils/base"
)

//socket接口
type RouterInterface interface {
	Route(controller, action string, params interface{}) (s base.SendData)
}
