// @version go-utils 0.1
// @author xiaotangren  <unphp@qq.com>
// @data 2014-07-21
package socket

import (
	"errors"
	"fmt"
)

var logsHander []func(s string)

var RouteMap map[string]RouterInterface

func init() {
	RouteMap = make(map[string]RouterInterface)
	logsHander = make([]func(s string), 0)
	logsHander = append(logsHander, func(s string) {
		fmt.Println(s)
	})
}

func AddModulesRoute(k string, r RouterInterface) {
	RouteMap[k] = r
}

func getModulesRoute(module string) (RouterInterface, error) {
	r, ok := RouteMap[module]
	if ok {
		return r, nil
	}
	return r, errors.New("Error::not have this modules![" + module + "]")

}

func AddlogHander(hander func(s string)) {
	logsHander = append(logsHander, hander)
}

func log(s string) {
	for _, hander := range logsHander {
		hander(s)
	}
}
