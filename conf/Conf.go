// @system SHOP-YUN <shop-yun.com>
// @version shop-yun 1.0
// @author xiaotangren  <unphp@qq.com>
// @data 2014-07-21
//
// 配置数据处理。

package conf

import (
	config "github.com/gokyle/goconfig"
	"strconv"
)

type Conf struct {
	Initconf config.ConfigMap
}

func (this *Conf) Get(section, key string) (data *confData) {
	// 日志数据保存路径
	d, ok := this.Initconf[section][key]
	if !ok {
		panic("conf not have the '[" + section + "][" + key + "]'")
	} else {
		data = &confData{
			Data: d,
		}
	}
	return
}

func (this *Conf) IsGet(section, key string) bool {
	_, ok := this.Initconf[section][key]
	return ok
}

type confData struct {
	Data string
}

func (this *confData) String() string {
	return this.Data
}

func (this *confData) Int() int {
	i, _ := strconv.Atoi(this.Data)
	return i
}

func (this *confData) Int64() int64 {
	i, _ := strconv.Atoi(this.Data)
	return int64(i)
}
